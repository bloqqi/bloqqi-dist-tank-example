#!/bin/sh
set -e # exit on error
java -jar tools/bloqqi-compiler.jar --dist=tank.json --o=gen/tank.c tank.dia
gcc -pthread -I../paho.mqtt.c/src/ gen/tank.c external.c ../paho.mqtt.c/src/libpaho-mqtt3a.a -o tank -lm

./tank $@
