# Distributed Tank Example


## Mosquitto (broker)
Install mosquitto (broker) using Docker:

    $ docker pull eclipse-mosquitto

Paste the following in a file ```mosquitto.conf```

    listener 1883

    listener 8080
    protocol websockets

This will make mosquitto listen on both normal tcp/ip (port 1883) and websockets (port 8080).

Run mosquitto:

    $ docker run -it -p 1883:1883 -p 8080:8080 -v <path>mosquitto.conf:/mosquitto/config/mosquitto.conf eclipse-mosquitto

Replace ```<path>``` with the absolute path to the configuration file. The example then needs to be adapted to use SSL when connecting and to the correct port.

### SSL
Mosquitto also supports connections over SSL. The following configuration illustrates how to add websockets over SSL when using letsencrypt.

    listener 8081
    protocol websockets
    certfile /etc/letsencrypt/live/<domain>/cert.pem
    cafile /etc/letsencrypt/live/<domain>/chain.pem
    keyfile /etc/letsencrypt/live/<domain>/privkey.pem

Replace ```<domain>``` with the correct domain name.

## Build Paho C library

Build Paho C library as a static library

    $ git clone https://github.com/eclipse/paho.mqtt.c.git
    $ cd paho.mqtt.c.git
    $ cmake -DPAHO_BUILD_STATIC=true
    $ make

This library should be placed ``../`` from this example for the build script to work correctly.

## Running Example

Run regulator:

    $ ./run-regulator.sh                # will connect to localhost:1883
    $ ./run-regulator.sh <broker host>
    $ ./run-regulator.sh <broker host> <username> <password>

Similarly for tank model (```run-tank.sh```).
