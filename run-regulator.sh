#!/bin/sh
set -e # exit on error
java -jar tools/bloqqi-compiler.jar --dist=regulator.json --o=gen/regulator.c regulator.dia
gcc -pthread -I../paho.mqtt.c/src/ gen/regulator.c external.c ../paho.mqtt.c/src/libpaho-mqtt3a.a -o regulator -lm

./regulator $@
