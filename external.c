#include <math.h>
#include <stdio.h>

int Real2Int(double in) {
	return in;
}

double SquareRoot(double in) {
	return sqrt(in);
}

void PrintLevel(double level, double setLevel) {
	printf("level: %.3f, setLevel: %.2f\n", level, setLevel);

}
